use std::char;
use std::collections::HashSet;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn load_words() -> HashSet<String> {
    let mut set = HashSet::new();
    let f = BufReader::new(File::open("src/10000_words.txt").unwrap());
    for word in f.lines() {
        set.insert(word.unwrap());
    }
    set
}

fn rot(msg: &str, rot_num: u32) -> String {
    if msg.is_ascii() {
        let mut output = String::new();
        for c in msg.chars() {
            let mut new_c;
            if (c as u32) > 96 && (c as u32) < 123 {
                new_c = (c as u32) + rot_num;
                if new_c > 122 {
                    new_c -= 26;
                }
                if new_c < 97 {
                    new_c += 26;
                }
            } else if (c as u32) > 64 && (c as u32) < 91 {
                new_c = (c as u32) + rot_num;
                if new_c > 90 {
                    new_c -= 26;
                }
                if new_c < 65 {
                    new_c += 26;
                }
            } else {
                new_c = c as u32;
            }
            output.push(char::from_u32(new_c).unwrap());
        }
        output
    } else {
        panic!("String sclice passed is not valid ascii");
    }
}

pub fn try_rots(msg: &str) {
    for i in 0..26 {
        let value = rot(msg, i);
        println!("{}", value);
    }
}

fn argmax(arr: Vec<u32>) -> usize {
    let mut max_idx = 0;
    let mut max_num: u32 = 0;
    for (idx, num) in arr.iter().enumerate() {
        if num > &max_num {
            max_num = *num;
            max_idx = idx;
        }
    }
    max_idx
}

pub fn find_answer(_msg: &str) -> String {
    let msg = _msg.to_lowercase();
    let mut output_candidates: Vec<String> = Vec::new();
    let mut scores: Vec<u32> = Vec::new();
    let set = load_words();
    for i in 0..26 {
        let candidate = rot(&msg, i);
        output_candidates.push(candidate.clone());
        let mut score = 0;
        for word in candidate.split(" ") {
            if set.contains(word) {
                score += 1;
            }
        }
        scores.push(score);
    }
    let max_idx = argmax(scores);
    rot(_msg, max_idx as u32)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_example() {
        let cipher_text = String::from(
            "se oy znk ykofk vgxoy nkxk! o nkrru utk lxoktjynov znk gtj eua sgsg tgvgrs noy ngtjrk",
        );
        assert_eq!(
            find_answer(&cipher_text),
            "my is the seize paris here! i hello one friendship the and you mama napalm his handle"
        );
    }

    #[test]
    fn rot11_example() {
        let cipher_text = String::from("piapctxpyelw awpmptly cpnzyyltddlynp");
        assert_eq!(
            find_answer(&cipher_text),
            "experimental plebeian reconnaissance"
        );
    }

    #[test]
    fn rot5_example() {
        let cipher_text = String::from("Rfm % dnjqi ymnx vz4wyJW nx XHMzbJJy!!!");
        assert_eq!(
            find_answer(&cipher_text),
            "Mah % yield this qu4rtER is SCHuwEEt!!!"
        );
    }

    #[test]
    fn rotten1() {
        let cipher_text = String::from("YRIRY GJB CNFFJBEQ EBGGRA");
        assert_eq!(
            find_answer(&cipher_text),
            "LEVEL TWO PASSWORD ROTTEN"
        );
    }
}
