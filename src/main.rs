extern crate clap;
use clap::{App, Arg};

mod rot;

fn main() {
    let matches = App::new("rotsolve")
        .version("0.1.0")
        .author("Kyle Harris")
        .arg(
            Arg::with_name("INPUT")
                .help("ciphertext to solve")
                .required(true),
        )
        .get_matches();

    println!("Using input string: {}", matches.value_of("INPUT").unwrap());
    println!(
        "Answer: {}",
        rot::find_answer(matches.value_of("INPUT").unwrap())
    );
    //rot::try_rots(matches.value_of("INPUT").unwrap());
}
